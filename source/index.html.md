﻿---
title: Tring ERP API

language_tabs:
  - JSON
---


# Overview

Welcome to the Tring Management System! If you want to integrate our service into your platform, then you need to contact us.
Contact: http://www.tring.al

> Request EndPoint :

```
tringuim.tring.al:8080/api/
```
# Distributor's API 

This sections explains the main operations that can be performed by an authorized distributor in the TRING Online Billing System

## Authentication

> You need a username and a password, given by the Tring TV Corporation:

```json
{
    "action": "status",
    "username": "xxx",
    "password": "yyy"
}
```

> If the authentication succeeds, the following JSON is returned:

```json
{
  "Message": "Valid Access",
  "status": 200,
  "UID": 5
}
```

> Otherwise, the following JSON is returned:

```json
{
  "Message": "Invalid Access",
  "status" : 403
}
```

Tring Management System uses a username and a password provided by the company to access the ERP. Please contact TRING to provide you these credentials.

`username: "xxx", "password": "yyy"`

<aside class="notice">
You must replace <code>xxx</code> and <code>yyy</code> with your personal credentials.
</aside>

## Contract Status
>The following json need to be encapsulated in a POST request

```json
{
    "action": "check_client_state",
    "username": "xxx",
    "password": "yyy",
    "scard_no": "xxxxxxxxxxx-x"
}

```

> The above command returns a JSON structured like this:

```json
{
    "contract": {
        "active_payments": [
            {
                "create_date": "2017-06-15 17:18:59",
                "current_active_payment": true,
                "end_date": "2017-07-15 17:18:59",
                "id": 32395,
                "packet": [
                    3,
                    "1 Mujor Tokësor i Plotë"
                ],
                "start_date": "2017-06-15 17:18:59"
            }
        ],
        "customer": [
            24313,
            "Terry Thomas"
        ],
        "id": 24206,
        "service": [
            1,
            "Tokësor"
        ],
        "smart_card_number": "xxxxxxxxxxx-x",
        "state": "active"
    },
    "status": 200
}
```

Checking contract's subscriptions is done by specifying <code>"action": "check_client_state"</code> and providing the Smart Card No.
This is an important step, as you may need to check contract's subscriptions and show these subscriptions to him.


<aside class="notice">
The <code>state</code> argument may have the following values
</aside>


State | Description | New Subscription Allowed
----- | ----------  | ------------------------ 
pairing | The devices of the contract are being paired | False
not_active | The contract does not have an active subscription | True 
active | The contract has at least one subscription | True 
expired | Client should renew his/her contract at a POS | False 
canceled | The contract has been canceled | False 

## Search Packets

```json 
{
    "action": "packets",
    "username":"xxxx",
    "password": "xxxx",
    "scard_no":"xxxxxxxxxxxxx"
}
```

To retrieve all available packets for a specific card number, specify <code>"action": "packets"</code>.

<aside class="notice">
The <code>data</code> argument is a list of json records of available packets for the smart_card of the contract. Each packet has the following information:
</aside>



> If everything goes well, the response will be similar to this:

```json
{
  "data": [
    {
      "display_name": "1 Mujor Tokësor Home",
      "duration": 1,
      "duration_type": "months",
      "id": 2,
      "offer": false,
      "packet_type": [
        1,
        "Home"
      ],
      "price": {
        "currency": "LEK",
        "value": 690
      },
      "service_type": [
        1,
        "Tokësor"
      ]
    },
    {
      "display_name": "1 Mujor Tokësor i Plotë",
      "duration": 1,
      "duration_type": "months",
      "id": 3,
      "offer": false,
      "packet_type": [
        2,
        "Familjar"
      ],
      "price": {
        "currency": "LEK",
        "value": 1450
      },
      "service_type": [
        1,
        "Tokësor"
      ]
    }
  ],
  "status": 200
}
```


Parameter | Type | Description 
----- | ---------- | ---------- 
display_name | String | The name of the packet 
duration_type | String | The unit of the packet's duration:  <code>days/months</code> 
duration | Integer | The duration of the packet in the specified duration_type above 
id | Integer | The id of the packet, used later to make the subscription 
offer | Boolean | It shows if the packet is an offer or not 
packet_type | List(Integer,String) | It shows the packet_type and the respective id. 
price | json {"currency": String, "value": Float} | The price of the packet in the currency of the POS 
service_type | List(Integer,String) | It shows the service_type and the respective id. 


## Subscription

>The following json need to be encapsulated in a POST request

```json
{
    "action": "make_subscription",
    "username":"xxxx",
    "password": "xxxx",
    "scard_no": "PK76HBQ3STPZ",
    "packet_id": 1,
    "payment_id":4
}
```

> If the subscription is successful, the following JSON is returned:

```json
{
  "Message": "You successfully made a subscription.",
  "status": 200
}
```

> If the packet_id is not provided correctly, the following JSON is returned:

```json
{
  "status": 400,
  "Message": "Packet is not valid."
}
```

> If the smart card is not linked to any contract, the following JSON is returned:

```json
{
  "status": 404,
  "Message": "There is no contract with this Smart Card No."
}
```

To make a subscription for a customer, specify <code>"action": "make_subscription"</code> and provide following parameters.


Parameter | Type | Description 
----- | ---------- | ---------- 
username | String | Username provided by Tring 
password | String | The password of the account 
scard_no | Integer |Smart Card Number of the client
packet_id | Integer | The id of the packet which corresponds to the <code>id</code> parameter in the previous request.
payment_id | Integer | A unique id provided by the distributor to identify the payment later.

## Check Subscription
>The following json need to be encapsulated in a POST request

```json
{
    "action": "check_payment",
    "username":"xxxx",
    "password": "xxxx",
    "payment_id":4
}
```

> If the payment request was successfully received by Tring, the following JSON will be returned: 

```json
{
  "payment_status":"success",
  "status": 200
}
```

> If the request was not received by Tring (e.x. because of a network issue), the following JSON will be returned

```json
{
  "Message": "There is no payment with this ID.",
  "status": 404
}

```
This section explains how to check the status of the payment made by the distributor. 

<aside class="notice">
<code>payment_id</code> is the id provided by the distributor for the payment in the previous request.
</aside>



#E-Store API  
## Log in 

> For the user to log in you need to provide the following JSON 

```json
{
	"action": "authenticate_user",
	"username":"e_store",
	"password":"******",
	"user_username":"xxxx",
	"user_password":"yyyy"
}
```
> if the user is logged in successfully

```json
{
  "client": [
    {
      "__last_update": "2017-03-2808:20:51",
      "birthday": false,
      "city": [
        1,
        "Tiranë"
      ],
      "contracts": [
        2
      ],
      "country": [
        1,
        "Shqipëri"
      ],
      "create_date": "2017-02-02 11:12:35",
      "customer_type": "individual",
      "display_name": "XXXX YYYY",
      "email": false,
      "first_time_login": true,
      "gender": "male",
      "id": 2,
      "image": "base64imagesample",
      "mobile": "+355*********",
      "mobile2": false,
      "name": "XXXX",
      "personal_no": false,
      "state": "subscribed",
      "surname": "YYYY"
    }
  ],
  "status": 200
}
```

> if the user doesn't exist in the system the following will be returned

```json
{
  "data": {},
  "status": 403
}
```
<aside class="success">
Request Explanation
</aside>


Parameter | Type | Description 
----- | ---------- | ---------- 
user_username | String | Username of the client in the system 
user_password | String | Password of the user in the system.


<aside class="success">
Response Explanation
</aside>



Argument | Type | Description 
----- | ---------- | ---------- 
__last_update | String  | Time of the last update for the client 
contracts | List  |List of the contract ids 
customer_type | String | Customer Type: individual/company.
first_time_login | Boolean | Shows if it is the first time the user logs in in the system.
image | Base64 String | It is a base64 representation of the image.
state | String | Current State of the client : without_contract,unsubscribed,subscribed





<aside class="notice">
<code>payments</code> is a list of self-explainatory records 
</aside>









## Change User Information

You can supply with any combination of the the following arguments inside client_data
> Request is as follows 

```json
{
	"action": "change_user_information",
	"username":"e_store",
	"password":"e",
	"client_data":{
		"id":2,
		"name":"XXXX2",
		"surname":"YYYY2",
		"mobile":"+355*******",
		"password":"******",
		"image":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAGBAUoDASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAQACAwQFBgcICf/EAEUQAAEDAgQDBQYEAwUGBwEAAAEAAgMEEQUSITEGQVEHEyJhcRQyQoGRoQgjscEVUtEkM2Lh8BY2U3J0gkNEc5KisrPx/8QAGwEBAQEBAQEBAQAAAAAAAAAAAAECAwQFBgf/xAAoEQEBAAIBAwQBBQEBAQAAAAAAAQIRAxIhMQQFE0EiFDIzUXFhI4H/2gAMAwEAAhEDEQA/AM7X1w7uwIWoYnUd5U2jOh3WUxKlqASLqlRYcZJxm3K55ZOmMZLBInvaLBbJCx7GZRdWMCw0MjAyrNuoWttcarMjVrAMbJfXdZKkcRa6smlAcbBGSmc2xCrK5BKLJ1Qxj2X5qkA5rLqJ80h0CKgqqVkjrWU1FQsYdBZGBrpH3IWWpocoGiQOijDGhFwU+VNLVRVcy5WZ4V8NY8dQFjSxZTh0Za35KxL4bglZEJLbAJIpIAkQilZQNQTkrIGlJFJA0oFOQQNQTkOagagnJIGWTVImlAyyBT7IEIIyECE8hNIQMITCFKQmkIiIhMIUxCYQioXBMspXBNsoOXYhQx+IOG6oUmGtbUZmBbhPSNlOoTabDw1+g0WLGzcLgLW2IV98V3KxDCGDZS5AqKXs1yDZSGnBFiFby2SIQY+SlBFrKD2EX2WVLUMqqqUNK1p2VpsYA2UmVOAREeRAsU1krKiv3avYM3LWtUGVW8NFqtiJW0jYIoN2CNltgkEbJIAkilZRQIQTkCgCCKBQAoFIoFShIFIpKBIJJIEhZFJA0hAhOKSojITSFKQmkIIymkKQptkRGQmEKUhNIRUJCbZSuCZZBge6bvZPa0DZOsjZYbNsiAjZEKgWSsnWRsgjslZSWSsgYGo2T7I2QMDUcqeAjawugjyqajsKlnqqVTOGA6qjS4m3+IQszal1lm5yNTC2Ohs90JyZCbxNPknrs4kkkkgSSSSgBQKJTSiwimlFAqKBQKKBUASSSQJIoJXRCSQJTS5A9AlMLigSVQ8kJhcgdk0oCSgSkggBQKJQKIYUE4pqDC2RsikstkAkAiEQikAlZEBFALJWTglZALIgI2RCKFkH+4U+yDx4Sg1zGZHMY6y0JtfO3iSjGYhvetH3XQMZZdjlzmsZkxumd0lb+q+b6rKyx7/T4yyvRFCb0sZ8lOquGHNQxH/CFaX1cfD5mXkklrfF3GmB8KwZ8XrWMkPuws8Ujv8AtH7rkOKfiDdUVEsPD2DOc0XyzVTt/PK3+qqPQJIAudAqT8Xw1jsr8Qo2naxmaP3Xk7H+0HiXHDJHitS0xHxMgjc1jdf1AvZazUVtZkLqzDoqiK4uBfMB5WNvpZXQ9r02LYdVuLaSvpJ3A2Ijma4g/Iq3cLwz7cTG2aATRtYbvyaub6g68uRWdwfjbEKYxRsr62OMaiUVL23Hlc25bFZXu9kFBcR4G7XGSvlixOodVxl4DMrAHx30A0JzA9d12DCMVpMWpRPQy527OaRZzD0cDqCou11BFAqAIJXTSUBJTCUHOTMyB5KCF0kBRQCKqAmlPTSgaUESgU0AgUUkDSmp5CFlUYZFGyVlh0KyICQCKBWSRsiAgFkQEQEQEUEbI2SsoEAg8eEpwSdsqMDi7fA5c6xduWvjd0eD910rFW3YVzfiWRlO4yyGzWm6+Z6yW60+h6ayTu7phNRGzCopJJGMY1lyXGwAsuEdsXbLN7RLgvBswyNFp8Ra6wv/ACsP78+S1TjbtIrsbwn+FQSSU1Ba0kUYAMg5Bzv1WgAx1FmVL4KWIa5Wi5t16k/RfWwn4zb5effKsXU1k9RUvqJ45aqaUkvkls0uPqf6LJPrqvuD3tPTwhoAAMhLgPRuyZSR0hq2x00k8vV7o+XkeSsyxMp5h3DJpWj3o3XcSbdP8ltGFqM7W957XHIQRazneH6qr7RVSSANnYf+dwsT+6zU1HO1olgpKxgcfijDR9wFAxr45WmSimkcObR4m+ellmtaYx0czH3dEA8Hdh0+iu0zi9oytzwuddw0dk+epB8iFkh7HMZQ6aZj7e5I57Tcc77fosRU0z4KnPTyCUkg27yx+4+6lGS7iSidmglEYmAHeWFj6HYHzW4cI8aYnw7ikE8FfU1MUYvLC5wvI3nrqOlt+a1uglhFK9kmaF1sx7x4Ldf3+ihZLGJWSQyWynwNcLtzciT08lN6XW3pzgHtPp8dnlixCamj1b3RaHNJuT4SDzGmuy6bmBFwQR5LwxFUT0kJfVljnwyDlmzjoeq7T2XdpFFJU09HVujpHSAMYWPLY3EDTQ6A+X0Tyz4d7c5RucmB+ZgcCCCLghMc5FPLkLqIuQzoLAKddV2uTw5BMCjdRApwKIeU0pXQKoRQKSSAIpJIAhZOKCIxFkgiksOhBGyQRCBIhIIoEkkiFFJFBIICkdkgkdlRiMUsI3E6ABcD48xR2J1lbDTOlkpaIEzZLDM7+W66n2n44KGjbh9LJavqvdA3a3mfJcSpaKpqJJ6anlkDIyc2lszuQ8ySpjxzfVWryXXTGqQCaSOV9QGtp89y0XOYjZu/+tVDPTTnNPG/upHG9287+a6RQ8FuqpIw8h4AFgNQ3yW7UHA1IImidrHEDm26mfNJ2bw9Pcu7hmFtrXBrszu81zHYjpY80+SnxeOQyF3eC/Ik/rcr0FHwjSQtsyGO3k2xQfwrh8hGeLxdbbrH6h1npXBA6vl3ika625GnoQqtQ11O/wDtdHlv8TBkcB66/dehP9lKFg0pGrHV/CdG5mVsNhvbks/qGv0jzvNNISWU9VO0cmklv7kfoond45uWfxHcOa1dureCoJBo0eQcwGyqR8EsZmzEWOlso0CfPD9LXFQZY3FzHuLgbg3IupYa2XO8FrQLX8zZdKxHhOOmie5tzc+7bZa5WcOwxDvYHtc48iQCPktY8srOXp7ixI7yth7wyDKwbOabH/VllaanjraQf+G5oEjA3k7YWO+4/RRwU8tFQTQuJjDhzJsPQDmrdHKQxrarwy+Bw0y2bewFvX9V1xu3lzxs8vQPYfxRU4zw+aPEZg+ppAGDN75bbn9l0hzl5N4bxWo4d7RKStpjlgklDJYwdHAmx+xuvVTn3AK0wkL03vNVA56aXapoXGPUrXKix+qssdogsNcpGlQNKmaqh90kEkQkkEkUUU0JyIKCSSDEpJJLDoKSCKApIXRQFJBJFG6KajdQEJs8rIYXyyuDI2Auc47AIrCcZtifw3W9+LtDLgXtqqVyPHDUYtj9birSbSXZC8/A0e6AOp3KjoMLq3AeNz5HXIu2wLv5tFsFI2FmFxzEEk3ys6af6+qznDUH5LZZR4j7qzy59MdPT4dVWcCwxlJSxtLdmjcLMtYDsLJN8TrW2U7Q46ALwXLb6uOOjMgB0TTDc+StmNuUXIDj5o92OoKul3FQxDL4RdVJ4WkG7bXWYjBy20I+4VWoZe+miumephXUzSNRqqktIOunos46MAkAqtURDKSNCVNNStNxelaGENZoRqtddQwOdaRuo1uOi3bEQSxw0uDqtaraYF5JFhyIUmWmrNxg8RwRksRkjB0Fx1/1quZmnNNX1EIIa0EPLue97rtMBaW93uLWXN+LcNeyue6I5Xk3FufL9F6eLLu8PPh2UquSaXu5Ysr3RhsgLhzvrY+Wi9Q8JYy3HeG6DEGEXmiBeBycNCPqvL8JNRCyGnB733m3+JzQbtPyuuu9hmKOdSV2Gu/u43d9F1AJ1afQr1R86x1dx80y6BKBKomjOqtxlUI3aq5EURZYVYYqrN1ZjREvJBFIoGpIlBAgnJoTkCRSSQYdFBBYdDkkLpIHJIXQuinXSumpXQOSBTQUgUDlrnaAx0vDU8bdS9zW2+a2K6w3FbgMNaHagyN/VWFc7xaWOhpKangF2tIuepW2YE4GjYQLCw0XMpquSrqo5JRZmc5Gb8zqumYHGW0UZcdwNl5vU16/SY92cj1NyN+inZcN2VaK+gGyusaTsLLyYx9C9ha6wFyNEHStAN3Ac9Ee6ICryRnUkG/kundjtVlkjb2zA/NMmsGfoqwuXEbqcAlgKu9sa1VUuaX21BPkoZHNLLG99lbkiN9rKvIyzSSppqMFiLPA8EDVa3WMNrblbXVMu6xPhOq12vZ4jYbfos2OkrEA5HnZarxlFmDZG3uNbhbTK3XTmea17iyF0uGvLSbs1BBXTjurHLlx3K1CKqjgrqafLpJo+219j9QV0DsuqmUnGTBDKDBiEBIbbZw1/b7rjsVa987oWkB2bNt8Q/YhbbwFVOp+K8Blzks74NsfhDiR/r0Xuj5GUeoboEppQJW2EkZ8SvQlY6M6q/CgtMVqNVWKzGiJxskkNkVA1BFIoAAiEgiECRskigwiRQSWHQSUk1FAUkEkUrpXSQQG6SARCArW+PWyOwQ92bEOvf5FbItf4xmY2jjhedZHbdbKwrhWAOmxPiU0xDnRxPyG2gDWrumHRCOFrbWsNlzbgnDBHxRiT2tBjDyWn11XUGkRMzOIaALkrx8/fLT6Hp9TDa3GGt1doPNXI5I22udCuaY1xSx1eYmucI4zoQdCdlhMY4xrYmMjpy8MvYutcnyVxxkayz27WJI7XJBHqmPdCdLjXzXADjvEdY4tpzXkusblhbb0K2jh6bF4MgrJJXucdHOvmaD1XTUYlrp0jA3UKdrQImXOywNBVvfOBJmu7l0WXrJO6gB6BY1I3u06ocwBpvyWKqqlmUm9isLjeOmnonOj8TgNLdVzXEjxRxCDFRRvEX848I+qskqXKzw6RX11M3TvmZulwtcr8XpsrnZxlF/qtPp+z/iYtz1ANyN++Nx8/wDJVazhDGaZ0zponO00LXl311UuMSclbXTV1NXRl1PIHEaEKnXxiWmmY4biy0Nk1ZhVcx0kMkbc1j0K3qGobXQskZ7rws9Ou7fXvs43iUBo8fAePi0Pqea2LhZ2TiLC3OIDhMLjl7wP7lN7Q8NfHilPJGLZ9furfD1Jnmw6T3e6mBJPIAgr04Xc3Xg5Mfysj1A03Y30TXFQUFXDWUjJad4cwjcKZdnCyy6p8W6yEOyoQ7rIQ7KotRq1EqsatRqInGyRSCKgaUCnIFAAnBNCcEBSSCNkGCSRQWHQkEUgECSRQRQKSJSCAIhJEIEFrfFVNHUSwmoeGMb7pPM3WyrD8Q0zKiBveC4a4OWM7ZNx39Phjnn05NP4cjY3iGsMYaBztzWy4xQz1lG+KnOUuFsywmCwCHiqtDW5Wd2LDzvqt0bnDLNbf5rz8l/Lb1ceOsNNS4e4Ow/DqT+3xtqakuzOkdtfy8kMT4p4bwVksdPC2pmYQx0dHEHlpJsA53ut+ZCyXEmD1GN0rqVtTJTQn3+6NnP8r8gsZh2AtocFmwefC46jDpNMjDld6+vNa47Le7OcsnZqEnaUKnGG4bBg00dQSA1rntedWgi4aTbQrbsIxqOsaYqimdFONwPEFiMG4RpsArpcQpaKY1DgQx9U8PLOWlgPusthFNO+tM0jn5s1wdBZOSSXs3xS2fk2jD4o7sdlBPIqXHcgpHuDhoOqhidl+ShxypaKF9zyXOZdnTo+3L5ZxVY2ymcSWOdqBzW1U1fLJUew4dG5pZZuWIAn/uds0fdaQXOp8V9pZu1+Zb3QUtGXNq6VpYZPES1xFirh5OTHTQOLuN+JMInnhiq6OGSCYwup3RvkfzsSToQQAbj+YKozjLH/AOGU1bilDBPDNc3gux413sdCF0HiLAYsWnZU1ENNVTMFg6VoLiOhPP5rXsZwmsqQGzyMZGAAGtaAAOg8l2zyxebDDKXvWGMtLj1KJmtzxk9NWlZPDsJ9kga1hd3fK42VjBOHmUzR3LZGM5nWxWUqT4Qwuddui4dT0dO+7nfaTS5W4bOBcMls7zG/7LA0olio2906we8kOA1Autx45gdLh0BBPglBJ6X0WEpoxVMfHFu3Yrt1fixwYb5equmdlL5f4VK2V7nAWIut5Wrdn9IaXBQXbuNvotoG69HH+14vV2Xluk8A1WQh2VKBXotl0eVZj5K1Gq0atRqCUbIpBIqAIIlBAgiEEQgIRSSQYPmkkksOhJJJIpJJJIAUgklzQJFBEIEFDWR95FbzUyTxdhClm46ceXTlK1uema3HRVRkC8eR7ep5FZ+lOZgKw9ZTZZRUZnBzCA4cisrRm0TT5Lx5fu7vp5ST9qd7b76HyUUjnAWErR6i6mLg5RPjDtSNlqdvDEkvlSkpe/deaUkeilEUEMRbG3Xa6n7m50UU0kMTDrmf0CnduSISAFjeI2j2FxHRXIpTKdrWVHH5G+yuYbg26KadZHLJPFK5pOt9FtnDdU6OEQyg5eRWnY02eGoY+Ee87VbLwhiENY51NOAydvXmpq7Mu87tklPeEhoKrOpczwHtBv11WRMBYSDZMfYNFr3CtY6ZUTckAsy4adCL6fRYXFS0yFzBv0U1dO5rst9FjJpC8tIOqzsuOoxfFDQcEq77NAP3WpcLkyPeWA22Hmt8xJjJIJmSAOY5tiDsVX4QwUPxaMBoEDfHYeS7+bI58XJMcba6FhEHs2G08RFi1gv6q+zdMItpyT4xqvVOz5GV6rtbhCvRDZU4Bqr0QWmFmMKyxV4wrLEEg2RKASUAKSRQQIJwTQnBAUrpJIMIUEUFh0JJJJFJJJAoEgiUEBvokgkgcEQgEUVSrAH0k7edrplI+9PH1srFTE4hxZbUbLGUbnMcWO5FeTmx13fS4s8cvDKxFWB0Kqwam4T3P7tpzHVTCt5RDidWIYvBobcljqGMyRumlvqfshWu7x7TJo2/1RfOI6YNZz5LcltOqYzUXKV9Ob5HAgGx6hV8TFNOx7QdhzWLEjjJnBtyKw9bUPlqpIYHuY4DxcxqtdCXLvtrGPVNNC58cj2iztCVXwDLNjkVRGS1oblv1VHH8HmLnSzkE5iW3T8EldSTWeCSp06bmbrMN3x2NydrqrWDISq9HWEwh7SLNbqFN7VFUxEx2JGhHMLOcMM2t4s4mQEaAaLGsku8ArKYsMvvWBWu99+aLH4tFxk7nJySzsuYm68E3itYbrYOz6mlkElW8ER5cjSeZVDh2jhxTF/ZquPPA4Eubfe3+a6M2GKmhbFAwMjaLBrRYBezjx3dvn8nL043D+1d26fFuo5CjEdV3eRkoBsr0SowHQK9EjKyxTtULFM1A8IoJIEgkUEBTgmhOCApJIqDBoIoLLoBSSSRSSSQQIoc0ikgSKCRQOCcCmBOCKLvdWFkGSsd/i1CzLvdWIrwRIx45GxXLlm47cOWslyN9m6DVUquV5msQcvNTwuGgOgTcVa1lOXNsCOYXHGaei5d2FxCqY2tYx7rWGjSp4wJfG0+A8wVizhftkzZKkZwbHXorc2CRtNqZ0jGkWyMeWhbmTphxy+avdxGW+OVgaed7LG02HuiqpXCz2OcCHXCimwRhYSJJg8fzOJWL/hEone11VMI3aZQ6wC3uu84MbPK1jsdC/M3vYs2xAdchaZXMZFK0xuabaaHks1iGA0scRGaSRx/xErT6rAYhOS9rt/dDiVjK68reLGTs3XBJGVFHaN93WLSQbqaibLTYtrm7mUEEKDhygZS0jHMYGW1IA0K2KDupmEuAa5vXks3u8mU6WvcQuAkIGruq1i4EoOwBWb4glAqHaEna61ypkDS3qVJGdt47OW97ik8g1DIz9yt9mWqdmdJ3WFz1LhrK7KD5D/+rapivVhPxeLlu8lSTdGN2oTZCmMd4l0c2WgdorsLgsVA/RXI3qIysblM0qhDIrTHoizdK6iD07MgckgCiCgITgmhOCAooIIMIUEUCo6EUkEVFJIpIXQI7JvNEoFAkUEUCCcE0JwQOOxWJxDQFZXksXiQ8BWM/DeHlWp5RZt99ro1V6kCPcLDwVBbLI038Ivccgr0dUPDzJ81xl29P+shGxkYAsBopnwhzPCbEKrE/ObX0HXkpHyOYbbq9LphkrVcU2QBryCsZLDUEt8ZtrdZaSZ0jXWYFTlYSQQDdadpv+2JnonyE3fYeepWOdhUUcrS5xc7qVlq+Z8bDa2o6LHtqnSuFg0E9VjLu3MatQAR2sQG7KhUVPcTluazWuLSepSnqGsD7E5raeS1jHMYjhhd4/EbHTcLElcOT/qPGK4TVDrG9ibn0WDildUTeEF2tmgblVZpZ6lhe8lrXG5PVbV2fYW6ed9dKz8iAHJf4nf5Lrjju6ebLLpm6qdnPatDR1U+DY7H7OxkzmxSW90X2cuzNqY6iJssL2vjcLtcDcELxlxlK08W4m6O1jM7b1XSeyHtC/h4ZhGLy/2VxtDI4+4eh8l7bhqdnh6t3u75K9Rtfqq4qGSsD43BzSLgg7pneWK5tMrE/ZW4pfNYeKYdVajmUGZilVuOXzWHhlCtxyIMm2RPD1j2yKVsqC6Hp4eqbZU9sgKIuNcpAVUa9SNciLCCjD0cyDEIFJJR0BJJJAQgkkigUEUECRQSQEJwTUQgcsbiI8JWSWIxuqgpIHSVEjWNAuSSs5d41jdUODKVlRiNc2QAgx5dfNYnEaCairJYju06eYUPZNxPT4zxhitLSOzRRQtObqbldE4iwkVsXext/NZ05hJxXHCRZyy53+mgU9Q6ORpmabdFmO8jkYMhuLKo+mtoRmA+oTGMfE6zfd6FY/16MbrwsAHPdoJaUHtDWkuNiNVVEs0Bc4NJF72BWNr66ofZrIXuJ8k7O3UkxVzHQ6aa2vbmsA68JdLcZW66q/IKmRrc0dzz5BYzFoql8ZDnBrT8IWbGvl7aYXH8VFLAS3xTSXsL81r1Jhz6q1RVk5QL2KzT8Oa6QSzZnuB0Fll8L4XxDHZGtY32ejB1eenl1TDG3ti4cmc81rdBhM/EOJR0NCCIWW72W2jB/Vbd2hYvR8D8IOip7MlLO6gZzJ5lb5h+E0PDmFmOmYGMYMz3ndx5kleTO17ip3E/E8ro3k0dOTHEOXmV7uPhmE/6+fyctzrSZpnTTvlkJLnkuJ80+N55FVualj3XRzdD4H7RMQwJzIKpzqmi2yuOrfQrteDcWYTi0LH01ZEHuF8jnWcF5ZbonPlfGWPjcQQdwbLOXHKTJ7BjnBsQQR5KzHP5ry5gfG+N4WWiGre+MfBJ4gugYJ2sRvytxOlLDzfGb/Zc7hW5lHcIajzV2KfzWiYLxThmKtaaOsjc4/ATZ30Www1XmsaVsjJvNStlWDiqr81ZjqPNRWXbIpGyLGMnUzJfNEZJkinbIsa2RTNkUGQD07OqLZE/vPNUQpJIKNkUEklQUCkkoAUESgikimhB72xtLnuDQOZQPSe9sbS57g1o5krTOLO0LB+H4nB87ZJuTGm64lxZ2oYtjrnxUrjS0x08O5C3jx3Ji5yO18W9o2E4FG9jJmzVIGjGm+q4HxbxpifEVQ908ro6e+kbTotXfI6VxdI4uedyTcqGaQNGUL04cUxccs7XWvwy4g2n7Qp6dxt7RTED1BBXrTkvFPYMXN7TcNe3o8H6L2pGbsCxyzuYMPjWCiqvNSkR1HP+V3qtXfFMx5iqIiyQciuhKvV0sNUzLMwG2x5hebPDfh6uPl6e1c/lpnkkj6Ku6lmcSO716rbajDnseBTujkF/iNiFdosLjaRJO5srug90f1XKYXfd6Ly4yNFGH1spAax2o5AlIcI11U68jbDq7RdOAAFgAAgdl2nHHny579NIw7gikpyH1f5zh8NrN/zWZmbFTxiOJrWgaAALJVcmRpWvYnUNhhlnldlYxpcSeQC9fHjJOzzZZ3Ly5J+ILjH+DYD/
AAykktWVgINjq1nMrysQSbnW62ztI4hk4l4qrK1ziYcxZEOjRstV5q1mI7G+ylbpuja+vNIqLtK1OcC5pag0+BObfLcbrbKOF99DuFOHEc1Wl8EgePddv6qZjha6xppdp6qSF7XRvLXDYg2IW88NdomJYdljqH+1QDTLIdR6Fc8B6p7XWUsl8rK9LcO8aYXjDWtinEU/OKQ2Py6raY6o9V5IgqHRvBBIt0W48P8AHuKYbljdN38I+CXX77rllx/01MnpCKq6lW46kdVzLhzjzDsUyxyu9mnPwvPhPoVuEVTcAg3B6LnZZ5abNHUeasMn81rsVV5q3HU+agzrZvNP73zWHZU+ak9o80GcQSSKjQJJIIooJKOaaOFhfK8MaNySgkUU0zIWl0j2taOZK57xn2pYVgYfFTPFRUD4Wm64fxT2k41jj3tE7oID8LCt48dyZucj0BxP2jYJgcbw6obLMBoxpuuF8YdqeLY1K9lLIaam2AadSFzyed8ry6R7nOO5JupIYgbEm916MeKTy5ZZ2pXPlqZDLUSOe483G6laLJbeiY+W2gXXWmD3vDRpuoSbi/VAm6B0Co6x+HOi9o40bNb+6aV68jFhZea/ws0IdUV1URt4QV6VGi4ct7tYGzysgidLK4MY0XJJ0C0DiDiyWoL4sOdkhGhfzctmqpocWmmopBmp/dIPxLnONYb/AArFZqNpJjNnRk/ylfK9xy5OPjlx8Pue0cXDycl+TvfpTfXVQBHfPsdfeKqScRV+D2qqaqka6/uEktPqE+sj8JsSCtV4glMkrIB4tQBpzXwuLk5OrtX6jk4eK4fljNO1cG8f4fjzWQVDm01cdCwnwuPkVuUjrNXPeyzDYKehu6KMyfzZRddDeGltidF+p4ZlMZ13dfhvVfH8l+KajF1JzGy5F+IXiP8AgXBzqWB+Wqrj3TbHUN+I/wCuq7DUxOaC4at6rxl2+8SnH+OKiKJ+alofyIwNiR7x+q9kvZ465k83OqZunHUoDdRRATtOaQSVQ/TLZOaco10CYDZVgHTPtL4QNmq7Ej5u9/LYMwvqeSma3KLJMjDQMo0TygQ2CcmHZEc0D2lEP81GEjsoqzHUPYfC4hbDhfGOL4e0Np62QMHwu8Q+61a6GYrNkpt1nBu1Orjka3EqeOaPm6Pwu/oumcP8S0GNw56CcOI96M6Ob6heXGyEbK3Q4hVUdQ2ejmfDKw3EjXWss3CVqZPWrKnzUntPmuJcNdq2V0NPjkILfdNTHv6lv9F1aKriliZJHK1zHgOa4HQg7FcrjY1LHS0EkFhslBU1UFMwunlawDqVhON+J6ThfBpayqcAQPC3mSvLvEXHmL8RVUk0lQ+KncTljabaLeOFyZyy075xb2q4Tg4fFTPFRUDSzddVxTiztJxnHHOYJjTwHTKw6rSHuc5xLiSTzKjcvRjxSOdztOlkdI4ue4ucdySoyihuummTCLqWnky+E8tkBHzKNrHRBO+UkJl9E07JBVDtkT7uqbzCLzog9T/hcoizhaoqSLB8pAK7PUzZn9wy+dw1PQLnnYCaePs7oIKXV4aTI7lmOq6PHC1ji4e8dyvPne+63jNRR9gihbdmhWn8cwn2ijqdwAYyfuF0F7ARYrVe0DuYsCcw2EpcHM06Lx+t1lwZbfS9uzuPqMdNAmAdGTZaJVNvjkBuMrp2jbbVbS+OrqaGOWGobHnYHWa0aXHUrUKyGrhxSldJIHAStcc40Ooubhfm+DtyR+x5Z/5ZO48IRSRRSlrja9gFtkYldq82C1vhmVsdIwG2puthbN3lzfwN1JX6/Hw/A538q1/tD4kj4V4TxDEXOHesiIjaebjoPuvB1bO+oqJZpTmkkcXuPUk3Xoj8UOPl1Lh+FscQZ3GZzejBo36m/wBF5xlNl1k1HHLvUZ3Thc26IC5UgsB5pENsgdE9LJckjklQwXunENd7wSI1SASKOR4BLHZgOqUcmbQizhunDw3INhzUFN4y6Q/EdFRPyRS5I2QJLqlb6pHmgA312TSjomuIsoFc3sN02R+RoF9k8CwzFU533cgBkN1k4sdxCKNkbKudrGgNADzYALD8075rKvoWgSgo6l+SnkcTs0leV3eX/wARePvruIfYIpD3NMNRfS5XNaUWhbborvaLVms4kxCYuvmndr5A2VOnP5TfRezCa7PPbvue9RlOeUxdEAlSR6C6iUrPdUDiTZBIpX1RS5JW0S5JBUO5hMk1cB1KIUtHGZ8QpohqXyNb9SoPbHYvhn8M4Bwxjm2fIwSH5rfAsbgFOKTBqOACwjia37LIONm3Xmy71uI5ZmtJubALkvaRjrqwPjpyf+HGP1K3Ti2v9noJQHZXP0C5LG8YhimoLmNO/IL4nuvPZrix/wDr9R7H6PH+fP68MjE00mHU8RFgyNrb9dFquLsEkhLXuabHdbjWi7Tlv6la7VxONzlDgAdF8eXVffx/LFs/AOOOxDDKGFji6oAMb/UG110ycd3Tx0zTq7V58ua4x2EyRfxSu71wJDC9l/XVbv2o8Q/7PcCYtieYCokZ3EH/ADu0H01PyX7D0efycUyr8J7hx/Fz3GPLna/xAOIePMSqY3XpoX+zw9MjNPublaK/UqaVxNy7fmeqitcr1V4AaOaciUeSICROo6onohzQIuB3sCk03F0styg9zY2Fx5fdJNKjnOYiJu7tT5BSsaGtsFHAwi73e87UqfkkA+FGyOzQlyVQgECbApFNc7RQAlNcAXWRc6yDNsxUUpXWYRdUX6lTyu1VdyUAaFPzBRlDVZrT6GhVMWdlw2pd0jP6K0uf9s/EUmBcMllObTVJ7sHpdebGbunXK6jybxSc1ZO7rK4/dSU39wz0VfH7uBcTckq1ALUzPQL2zy8/0BNyg4pbFByoTRdPboExmie3ZA4oG90TsmnYICPNEJBInVUE6eqy/AtP7XxrgkFrh9XGD/7gsOdluHY1T+09p+AMttUZvoCVL4V7libZjGjkLJlU/KyynaLBYbGqoQMe4n3QvHllMZuvRw4XPKRp/F8rqiWSJliGi2vVaRgdM+lmmMjQHcjfQLZaiQSyvcQ5xJvqq72C5Pd63X5b1HJ8mdzftvTT4uOcapUkuzXNwsFX2ZSzyXIAY4/ZZqaxvYWWE4icY8FrHgbRO/ReaeXtx7RiezeodRVkMpGXPERcc+axn4keJ/bKnC8Bp33jp2e0TgfzuHhB9Br80op2YXhcFS8lojaCfouRY5iM2L4tVV9SSZJ3l2vIch8hov1PtV3xX/X4/wB9xk55f+MZIRtumgaIu1ci0L6n2+GACcAl6pXQA7pqdy80rc0A1CgH50v+Bp+pTp3E2Y0+J32CmjYGMsLIpW6JwuRskBqjsN0QOWyXIpHZHSxVDCmcii8+K1lE54DHFZCcc1mjcp0pDW2CZTg5M557JkhJJRUbje6ZZSBpKT25QoIXnRC6a86oXWar6HLzp+ILGjPxJT4cHflwMzEeZXooryN2wVBqOPcRfe4Y4M+gXLhm8nTk8NIxqMuhcR6p1C/PRRHyspZbTQEeSbRsEdGxvRerXdxA7pjlImEaoHWsEWG4S5JNHRUOPmkdkdEDqgSB3R3CIQB3urof4fIe+7U8LNvcD3f/ABK5673SurfhjgEvaSH/APDp3n9FnLwR7CccrCT0Wk8TVBcA1pHiPPoFtuJyd3SPOxK55jM4mq3WBcGDKF8f13J0cWv7fa9s4urPq/pjHG5P5gBPRROcOT77lSG/JgUMpNtWAaL87X6jGKk40Ot1r3GjhHw3VG9i7KwedyAthl1A6rVO0SXu8DhZa/e1EbPvf9lMJvKPTbrFpfaPWimwGlo2kZ5yNv5R/oLl7zpZZ7jHEjiOMSOabxQjuo/Qbn6rX3HRfr/QcPw8Ml83u/De6eo+f1Fs8Tsj5pw8kE9osF7XzSJ0TSByTtSkUDLISPDGknYKQiwVe3ezZfgZv5lAaZhN5HjV32UwBv8ANHTYJDQJoIJFIFIlAD1R5hAnXZNe8jmgqVUuWU+ia0F8bWfzG59FWkcZap19hushA3wFx0vp8lmd1J5+Fo0Hko8vW6nDeqa5KiMbKCZyle6wVd5uVKqI6lBEpqwr6FzyCKF8jjZrWkleOOMaplfxNic7Dma+dxB8tl68x2WOHB6x8xyxiN1z8l4oryY6uYxnMzObHqLrPp/utciMtLHGykA/JamMlEg81I0WiC9TkhIQtzTnJDZQBFmyRQaUD+aB5JJIpDyR5oWRG6Bcjddq/ChTGTjDEJ7aR09r+pXFTzPkvRP4Q6S7ceqyPiZGD9SsZ9os8u6cU1Qp6TfVc/kfe5fJYk3sFneNK3vq4U7QXBp1A8lr7i4DwsHldfmfceXqz6f6frPbOHo4pb9onFtj4zsoJSBezj0Vh2fo3f7KtKTzbqvl19fCd0Em91zfthrvZ6TDo2vIcZHyAeYbYH6ldDebHRcY7Y65s+O09M3U08Xi9Xa/sF6/b+H5eeRw9y5/g9PllPLnzySdfmo979E5x00QtdfsX4O3fc1u6fvohl0ujayIW26IsTdIapOs35IIZ3ljfDq46NCcxgijDR019UyEd5I6U7bN9OqdKblA5g1KLgbIR6AJPIHMIDbbVI7KPvG2te9k18wtoE2JH2AVSoks0p8htkub3KpVz7MOqzasRU3iPm532WWbcN20WPw5lmB7traKxLOT4Y0ngqZ8gaNSoXS32BUYA+M3PQKUA8hYII3XI2UMgsbKeV2UearPJJUpDCm/NOKFlhXtHtpxE0HBFVkdZ8tox815YfZ4Xoz8RDy3hWmA2MwuvODTqr6f9pyeVaaMtJcw2IU1LIZKcl3vA2Tj0KbTgNbIB1XbXdgSlsU5N6lUAnVIaJBJ24UDuSRNrIDZLlsgO5SvZJJFJ+116m/CnTezcBYlWOFhLVO18mgLyxLYNXqvshmGE9gsU7BaSbvC0dXOcQFx58unG114cOvOYxenqjWV9TUGSzS4gD0Kb4CdXOOtt1FRMdFTsaGC9tSSpiX/AMo5r8by59eVy/t+648OjGYz6NJZp4jzKrS+TrqwS62rReyry7G4XGu+DGYlOymp5ZpXZWMaXE+QXnDHa92J4tVVkl7yyFwvyHIfRdg7WMUNFgBp2OAkqnd2OuXc/wBPmuHvPIL9F7NwdOF5b9vzfv8A6jqzx4Z9GcykN7BHqgBr5r7b86cEiDZJEa3KoQ0CgndnIibu73rcgppHBjC5x2F1HBELF77l7tT5eSgcXMjZbpyVZznvf4Y3fPRXHho2FlHu64QRhshtmIHpqkWAGxu71U3yUb7kpoQuFh0Cjtp1UrwoZDpZQKoNnM+qx9a7MAPNW6g+PTkFRks+VrXbXuVitRZjfm8LASByCssgcR4zlHQFNie1kYAytHmniQOOl3LURIGNbo1CQ6GyILuYACje+4sERWk13TU6Q2KjJ0Wa0a4pqNrpLI9XfiIroRgdLQkgzPkzgei85Oe6N2uy3ztZx52M8cVTWvJhp/yma6X5rR5RqQVrix1imV3TBM089U+nIL3DqFXfED5JlMHR1LfES06LptF11wCm7BSPFlGddFqoDUibm6LUrKBA7p3JMCdyQEDRBJpsgUU2c2jPovT3D8wh7J+DsPLshmb37x/hbr+pC8vVJtC7yC9HcOVJkwzh2ma0OFNhkI15F3iP2svne5cnRw19T2ji+T1E39NpjLABd7if6J3gA982AQYX5b5G7dU67ubBuAvyj9ga6wvZ56KCU76qZ5uBdvNY3FqplFQT1MmjImOe70Av+yknVdRqWYzdcQ7VcT9t4kdCx146ZuTyzHU/sPktIPVWcRqX1VZNUSG75Xl7j5k3KqXuV+39PxTi48cJ9PwPqua83NlnfsdzZOCYN1IuzzklayA3umzPLIyRq46AIIyDNLYe4zfzKn5W6JsIEcYBIvuTdNMjeRv6KB7imA2KaZCbWaSmgvOwAREt7KNzup0QIPNyYWhFMe8XNtVA9x5201U7+aqynRZqopXkklVIw6SbQ2AUsxsCo6N1naAk3WL3q/TKU9MwWLvEepVrKGgACyqxyPI2CfZ51c7RbZPfrzUMpyN03Se4NGirSSEnXVNqFiTcoOsELuKGW6zVAuvsELFSAAJXCyNvxn/eXEP+of8A/YqnUe8kku2PhhCdlGP71vqkkixdl3UPMpJLVQWojZJJIAPeKd19EklABuEj7qSSKgq/7h/ovQfBn/lv+kpv/wAWpJL5Hu/8T7nsX81/xuzf7v6p43+aSS/NP1BsnuLVe0P/AHSxL/0SkkunB/Lj/rPN/Fl/lecZNyoTuEkl+3nh/PqlZv8AJHqkkqhFVq7eL1/ZJJSgw7fIJ/JJJAOqaNwkkgcExySSUQuVaXmkks1VKfYpUO/zSSXP7a+mVp/dUkvupJLqwqTbKEpJKKdyQ5pJLNU1MO6SSyr/2Q==",
		"email":"*****@email.com",
		"personal_no":"J***********",
		"gender":"male",
		"customer_type":"individual",
		"country":1
	}
	
}

```

> If the request is successful, the following JSON will be provided

```json 
{
  "Message": "Changes are succesfully saved",
  "status": 200
}
```

<aside class="notice">
<code>image</code> is a base64 encoding of the image. To understand how this works, check the following website: https://www.base64encode.org/
</aside>

## Get Contracts for the client 

```json 
{
	"action": "get_contracts_for_client",
	"username":"e_store",
	"password":"e",
	"client_id":2
}
```

> If the request is successful, a list of contracts will be returned

```json 
{
  "contracts": [
    {
      "city": [
        1,
        "Tiranë"
      ],
      "country": [
        1,
        "Shqipëri"
      ],
      "create_date": "2017-02-02 11:13:11",
      "current_subscription": [
        1,
        "3 Mujor Tokësor i Plotë"
      ],
      "damaged_device_tickets": [],
      "display_name": "XXXX YYYY - Tokësor / Satelitor",
      "id": 2,
      "payments": [
        {
          "create_date": "2017-02-02 11:19:32",
          "current_active_payment": true,
          "end_date": "2017-04-05 10:19:05",
          "id": 1,
          "packet": [
            4,
            "3 Mujor Tokësor i Plotë"
          ],
          "start_date": "2017-02-02 11:19:32"
        }
      ],
      "smart_card_number": "xxxxxxxxxxx-x",
      "state": "active",
      "tickets": [],
      "update_client_data": true
    }
  ],
  "status": 200
}
```

Parameter | Type | Description 
----- | ---------- | ---------- 
user_username | String | Username of the client in the system 
user_password | String | Password of the user in the system.

<aside class="notice">
Please refer to the section <a href="#contract-status">Contract Status section</a> to see the available values for the <code>status</code> argument. 
</aside>




